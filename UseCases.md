# Use cases with Business Needs and High level requirements

This Document describes the Use cases and the associated Technical requirements. This list help to build the SylvaStack roadmap.
The priorities have been validated in the Technical Steering Committee


#### 1. 5G Core
  * CNFs of the 5GCore : UPF, SDM, UDM ....
  * Requirements : Hugepages, SRIOV, DPDK
  * Requirements covered by the release V0.3 of SylvaStack
#### 2. Open-RAN NF
  * Support CU & DU CNF
  * Tec Requirements : RT OS. PTP best practices.
#### 3. Tenant isolation optimizing baremetal resources
  * hard multi-tenancy (separate cluster for each tenant) optimising the usage of baremetal infrastructure
  * Technical requirements: support of Sylva deployment over KubeVirt or other tools to manage microVM over K8s
#### 4. PaaS for CNF LCM (Service Mesh, …)
  * Tools to manage the LCM of CNF
  * Service Mesh, egress control
  * CNF Log & Monitoring
#### 5. NaaS (include CAMARA API integration)
  * Test the integration with CAMARA API.
  * No specific technical Requirement identified yet
#### 6. Network Automation
  * connectivity between some 5G Core CNFs and the transport network many configurations (VLANs, BGP sessions, separated routing domains,…) are required on the datacenter fabric and therefore there is a need to include in the Kubernetes infrastructure automation system (e.g. Sylva) the operators able to perform also networking configurations.
  * requirements : declarative management of VLANs, BGP sessions, separated routing domains
#### 7. Edge VR (support of GPU)
  * support of GPU Acceleration
  * Test the integration of VR
#### 8. Edge Federation (interlock the edge Federation solution with the Telco Cloud Layer)
  * Test the integration with CAMARA API.
  * No specific technical Requirement identified yet
#### 9. IT Convergence (app Isolation)
  * IT & Network Convergence
  * Teq Req : Application Isolation, VCluster or alternative support
#### 10. LoadBalancing Private & Public Cloud
  * Hybrid Cloud. For example Extend in a Peak of Traffic the private TelcoCloud to hyperscaler.
  * Common Management Cluster to manage Private & Public Cloud CaaS.
#### 11. SDWAN
  * Support SDWAN Network functions
  * Tec Requirements : Kubevirt, specific network attachments
