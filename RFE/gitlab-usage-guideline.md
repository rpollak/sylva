# Enhancement/Feature description

There is a need to track, organize and efficiently triage the various issues that have to be developed and delivered for the Sylva releases.

This RFE is based on the following assumptions: 
- Gitlab are used to track all types of evolutions required 
- Gilab issues are created in the various Sylva repositories used to build the stack
- Issues have different types: features, maintenance, bugs, etc

Here are the identified requirements:

- REQ1: Identify the type of each issue (feature, bug, enhancement, cleanup)

Implementation: Scoped label: `type::feature` `type::bug` `type::enhancement` `type::cleanup`

- REQ2: Identify the status of each issue
    - issue is to be triaged (bug severity, priority, milestone, etc need to completed)
    - issue is open and not assigned 
    - issue is assigned: the issue is assigned to someone 
    - issue is assigned and work is ongoing
    - issue is closed 

 Implementation: 
    - Scope label: `status::to-be-triaged`
    - Scoped label: `status::ongoing`
    - We don't need labels for the rest because Gitlab supports searching issues by "Assigned = Any" or "Assigned = None", including for dashboards

- REQ 3: Relevant information to triage bugs can be added:
    - 3.1 Severity of the bug: used to determine urgency and define the impact on users. Description of the issue shall contain the reasons why severity level was chosen based on bug impacts.
        - Severity 0: the bug has severe impacts and there is no workaround available
        - Severity 1: the bug has severe impacts but a workaround is available
        - Severity 2: the bug has limited impacts (a workaround may or may not be available)
        - Severity 3: the bug has no visible impacts for the customers    
    - 3.2 bug cannot be reproduced
    - 3.3 bug is a regression
    - 3.4 bug has an identified workaround
    - 3.5 which sylva versions are affected by this bug: That includes released versions and unreleased version (e.g. bug breaking only main/master branch but not released versions)
    - 3.6 who/which component is impacted by the bug

Implementation:
    - Scoped label `severity::0` `severity::1` `severity::2` `severity::3`
    - Label `cannot-reproduce`
    - Label `regression`
    - Label `has-workaround`
    - Set of labels `affects-<release-version>`
    - Labels
        - `impact-infra-admin`
        - `impact-workload-cluster`
        - `impact-dev`
        - `impact-ci`

- REQ4: Identify prority of an issue. This is used to indicate the importance, to drive the scheduling and ensure the completeness of a future release:
    - high: the issue is high priority (e.g. feature being a release blocker, bug to be fixed quickly)
    - Medium: the issue is important but there are some higher priority issues
    - Low: the issue is low priority, we don't know yet when it would be implemented 

Implementation:
    - Scoped label `priority::high` `priority::medium` `priority::low` 

- REQ5: Relevant information to triage issues (feature, bugs) can be added:
    - 5.1 issue (feature or bug) depends on an external development not available yet 
    - 5.2 issue can be addressed by a newcomer developer to get more familiar with the stack
    - 5.3. issues relates to a dedicate area of the product

Implementation:
    - Label `external-dependencies`
    - Set of labels (open list) 
        - `documentation`
        - `capd` 
        - `capv`
        - `capo`
        - `oci`
        - `kubeadm`
        - `CI`
        - `baremetal`
        - `rke2`
        - ....

- REQ6: Identify issues which are included in each (future) releases. These issues may come from the collection of repositories used to build a Sylva release.

Implementation:
    - For features / changes / bugfixes not released yet, usage of Gitlab Milestone feature defined at the Sylva-Project level to track the `x.y` versions for which the feature is targeted 
    - Usage of labels `delivered-in-x.y.z` label(s) to track in which version(s) the change was delivered. There may be several versions if we have to do backports to maintain stable branches.

- REQ7: In case we have stable branches for release maintenance, identify the issues which are subject to backport and for which stable branches it has to be backported
    - N/A for the time being as we don't have stable branches 

- REQ8: The labels described here should be reusable in any sub-project of Sylva-projects group (and _will_ be used at least by `sylva-core` and`sylva-elements` sub-projects)

Implementation: All labels described above at created at the Sylva-project level

# Technical implementation

See inline in previous section

# How to test it

Not Applicable
